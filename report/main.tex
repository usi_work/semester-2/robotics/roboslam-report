\documentclass[10pt]{article}
\usepackage{2022abstract}
\usepackage{amsmath}
\usepackage{enumitem}

\setcitestyle{numbers}

\title{Monte Carlo Localization}

\author[1]{Joseph Romeo}

\begin{document}
\maketitle
\begin{multicols}{2}

\section{Abstract}

In this report I will describe my attempt at implementing the Monte Carlo Localization (MCL) algorithm in Coppelia\cite{CoppeliaSim:online} simulation
on a Robomaster EP \cite{RoboMaster:online}. I will start by briefly discussing what MCL is and why it is used. I will then discuss what the inputs and outputs
of the algorithm are. After we will look at the various parameters that can be used to tune the MCL algorithm. Finally, we will see the additional
components that are needed to make MCL useful.

\section{Purpose of MCL}

MCL is a localization algorithm that can be used to estimate a robot's pose given a known map, and the knowledge that the robot is somewhere within that
map.

\section{MCL Inputs}

The MCL algorithm requires 3 main inputs: odometry, range measurements and a known map. To obtain the odometry, the robomaster
ros \cite{RobomasterRosRepo:online} package was utilized. The package publishes odometry data on the /odom ros topic.

For the range measurements, I replaced the arm on the robomaster EP with a laser scanner. I set the laser scanner to scan 5 points: 1 facing straight
ahead, 2 facing the front at 45 degrees from center on each side, and 2 on the side at 90 degrees from center. See figure \ref{fig:robo_laserscanner}
for an illustration of the configuration (note it only shows one side). For MCL, I only used the sensor in the front. The additional sensors were used
for obstacle avoidance.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/laser_scanner.png}
\caption{Robomaster EP with Laser Scanner}
\label{fig:robo_laserscanner}
\end{figure}

The final required input to the MCL algorithm is a known map. To generate the map, I added an orthogonal type vision
sensor above the scene. To get images from the camera I added a lua child script to the vision sensor which captures grayscale images and
publishes them to a ROS topic as a ROS message type sensor\_msgs/Image. With the help of a library called CvBridge \cite{CvBridge:online},
I converted the image first from a ROS image to an openCV image and then finally into an occupancy grid. See figure \ref{fig:scene_and_occupancygrid}
illustrating this process.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/known_map.png}
\caption{Left: Scene with Vision Sensor, Right: Occupancy Grid}
\label{fig:scene_and_occupancygrid}
\end{figure}


\section{MCL Outputs}

The main output from the MCL are particles. Each particle represents a guess of the robot's location and orientation. After diving into the
details of the implementation of MCLn, we will see how we can use these output particles to make a guess of where the robot actually is.

\section{MCL Steps}

The MCL algorithm follows these steps:
\\
\begin{enumerate}[nolistsep]
    \item Generate random particles
    \item Update particles
    \item Observe/sense
    \item Resample
    \item Repeat steps 2-4 forever
\end{enumerate}

\vspace{10pt}

In the following subsections we will take a detailed look at each step of the algorithm, and the implementation choices I made.

\subsection{Generate Random Particles}
\label{sec:generate_random_particles}

Generating a random particle is a 3 step process. First we generate random x and y coordinates, pulling from a discrete uniform
distribution. The bounds for the uniform distribution are 0 and the width of the map for the x-coordinate, and 0 and the height of
the map for the y-coordinate. Next we check that the particle is valid (it's not in an illegal position, such as inside a wall). If it
isn't valid, we continue attempting to generate a random x,y coordinates until we find a valid one. Next, we generate a random orientation
for the particle, pulling from a continuous uniform distribution from 0 to 360 degrees.

We continue generating particles in the way described above until we reach the desired number of particles.

To put it more formally, at the output of this step we have a set of particles \(X_t = \{x_t^0, x_t^1, ..., x_t^n\}\), where t is the current
time, n is the number of particles, and each particle is represented by a tuple (x, y, \(\theta\)).

\subsection{Update Particles}
\label{sec:update_particles}

The next step is to update each of the random particles based on the motion of the robot since the last iteration of the algorithm. To calculate the motion
of the robot we take the current odometry measurement and compare it with the previous odometry measurement, getting a tuple (x,y,\(\theta\)) describing the motion.
The output of this calculation is a homogeneous transformation matrix as seen in \ref{eq:transformation_matrix}.

\begin{figure}[H]
    \begin{equation*}
        \mathbf{T} =
        \begin{bmatrix}
            \cos(\theta) & -\sin(\theta) & x \\
            \sin(\theta) & \cos(\theta) & y \\
            0 & 0 & 1
        \end{bmatrix}
    \end{equation*}
    \caption{Transformation Matrix}
    \label{eq:transformation_matrix}
\end{figure}

Also, to capture the fact that the odometry measurements are not 100\% accurate in the real world, a small amount of noise is added to the motion.
The noise for both the x,y coordinates and \(\theta\) were generated from a gaussian distribution. The actual values used and some illustrations of
the noise model can be found in the \nameref{sec:tuning} section.

In the end the output of this step is a new set of particles. To put it more formally, the step can be described with the following formula:

\begin{equation*}
    \mathbf{X_t} = updateParticles(u_t, X_{t-1})
\end{equation*}

Where \(X_t\) is our new set of particles, \(u_t\) is the transformation matrix describing the motion of the robot since the last time step, and
\(X_{t-1}\) is the set of particles from the last time step.

Below we can see a visualization of the move step in action \ref{fig:visualization_of_move_step}. On the left we can see the initial position of
the robot in red, and the particles in blue. On the right we can see the robot has moved forward while turning to the left. We can also see
that the particles all followed the same motion.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/sense_visualization_combined.jpg}
\caption{Visualization of the Move Step}
\label{fig:visualization_of_move_step}
\end{figure}

\subsubsection{Sense}

In the sense step of the MCL algorithm, we use the distance measurement coming from the laser scanner as input. Each particle also generates a distance
measurement, and then each particle's measurement is compared with the robot's distance measurement. This comparison is used to generate a likelihood
for each particle. This comparison is done using a gaussian probability density function which is described more in the \nameref{sec:tuning} section.

One of the biggest challenges I had in this step was generating distance measurements for each particle efficiently. In my first naive approach, with the
help of plotly, I used the particles location and orientation to generate points along a line in the direction the particle was facing. I then checked each
point along the line until either the edge of the map was reached or an obstacle was detected. In this case the map is smaller than the distance of the range
scanner, so I did not have to take the range scanner limits into account. This algorithm proved to be very slow, so I looked into alternative solutions.

I ended up using an algorithm coming from computer graphics called DDA (Digital Differential Analyzer). It is typically used as a line drawing algorithm, where
given 2 points on the screen, it determines which pixels are on the line between the 2 points. In our case, each cell of the occupancy grid can be thought of as
a pixel, and instead of filling in each pixel to draw a line, we check each cell in the occupancy grid to see if it is occupied or not. In an effort to save space
in this report, rather than detailing the algorithm fully, I will leave 2 excellent resources that I used to understand the algorithm myself: \cite[Description]{DDAWritten:online}, \cite[Video]{DDAVideo:online}.

Just to restate, now we have a method to calculate a distance measurement for each particle, and we have an actual distance measurement coming from the robot's
laser scanner, and we use these measurements to generate a likelihood for each particle. These likelihoods are then normalized so they add up to 1. To put it more
formally, the step can be described with the following formula:

\begin{equation*}
    \mathbf{w_t} = sense(z_t, X_t)
\end{equation*}

Where \(z_t\) is the distance measurement coming from the laser scanner at time t, \(X_t\) is the set of particles at time t, and \(w_t\) is a set of normalized likelihoods for
each particle, and sense is a function that generates these likelihoods given the inputs.

Below we can see a visualization of the output of the sense step \ref{fig:visualization_of_sense_step}.
The blue dots represent particles, and the size of the dot represents the likelihood of that particle.
The red dot is considered the robot's ground truth location. The green lines represent the distance
measurements from the particles and from the robot. If you look closely you can see that the particles
with the larger dots have distance measurements closer to the robot's distance measurement.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/sense_visualization_0.png}
\caption{Visualization of Sense Step}
\label{fig:visualization_of_sense_step}
\end{figure}


\subsection{Resampling}
\label{sec:resampling}

The final step in the MCL algorithm is to resample. The goal of the resample step is to create a new set of particles by sampling from the existing set of particles, replacing
samples with low likelihood with samples that have a higher likelihood. In the end, I chose to use an algorithm called low variance sampling to accomplish this. The algorithm
is easiest to understand with an example, so I will walk through a simple example. Remember the input to the resample step is a set of likelihoods for each particle. Imagine
we have a set of likelihoods like seen in the figure \ref{fig:low_variance_input} below:

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/low_variance_example.png}
\caption{Likelihood Input for Low Variance Resampling}
\label{fig:low_variance_input}
\end{figure}

In the figure, we see we are supplied likelihoods for 5 particles. We can read the figure as particle 0 has a likelihood of .1, particle 1 of .2, particle 3 of .1, etc. From
here, we reformat the likelihood set so that each item in the set shows the summed likelihood up to that point in the set, rather than the likelihood of one particle. See
figure \ref{fig:low_variance_summed} which shows how this would look based on the likelihood input in the previous figure.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/low_variance_summed.png}
\caption{Likelihood Summed}
\label{fig:low_variance_summed}
\end{figure}

Next we will sample from this set of summed likelihoods at even intervals. To calculate the interval, we use the number of particles. In this case since the number of particles
is 5, we will sample at the interval 1/5 or 0.20. To avoid starving particles at the beginning, we will start the first sample from a random number from a uniform distriubtion
from 0 to our interval size, in this case 0.20. Figure \ref{fig:low_variance_choice} shows what this would look like if the first sample was randomly chosen to be at 0.15.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/low_variance_choice.png}
\caption{Example Low Variance Sampling}
\label{fig:low_variance_choice}
\end{figure}

In the figure above, we see that the algorithm successfully removes 2 low likelihood particles, while also copying the particle with the highest likelihood 3 times.
This concludes a simple resampling step, but one additional step is needed to combat a problem called particle deprivation. As you can see, we chose 1 particle 3 times.
After a few more iterations of the algorithm, it is likely that all of the particles will converge on the location of this particle. If this particle happened to be a
bad guess for the robot's pose even though it had a high likelihood (ie, both robots looking at different walls from the same distance), then the algorithm will
converge to a bad guess, and it will never be able to escape that bad guess.

To combat this issue, instead of using low variance resampling to sample n particles, where n is the number of particles in the belief set, we sample m particles
where \(m < n\). We then regenerate random samples using the algorithm described in \nameref{sec:generate_random_particles}. These randomly generate particles help
both fix the issue where we converged on a bad guess, and additionally helps if the robot is kidnapped after it has successfully localized. We will discuss this more
in the \nameref{sec:tuning} section.

\section{MCL Tuning}
\label{sec:tuning}
There are many parameters to tune to that can help MCL perform better. In the following subsections we will discuss the most ones.

\subsection{Movement Model}
The first parameters to tune are in the movement model. As mentioned in \nameref{sec:update_particles} section, noise is added to the
position and orientation of each particle during the movement step to simulate the imperfections of odometry in the real world.  Before applying
the transformation matrix calculated from the odometry for the robot to each particle, I moved and turned each particle slightly.
To determine how much to move the particle by, I used a normal gaussian probability density function with a mean of 0 and a standard deviation
of 0.5 centimeters.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/movement_distribution.png}
\caption{Gaussian Distribution for Movement - x,y}
\label{fig:distribuiton_movement}
\end{figure}

In the above plot with have red lines drawn at 1 and 2 standard deviations from the mean. As we can see, ~68\% (1 standard deviation) of the
particles x and y coordinates will change by less than 0.5 centimeters, and ~95\% (2 standard deviations) of the particles will change by less
than 1 centimeter.  The noise amount is very low because the MCL algorithm is running at approximately 20hz, so the amount of distance traveled
between each step of the algorithm is very small. This is also why I chose to use additive noise versus multiplicative noise. If instead the
algorithm was running at a much lower frequency, and the robot could potentially drive high speeds, I might have opted for multiplicative noise
to make the amount of noise added proportional to the distance traveled since the last step. In my case additive noise was sufficient.

A similar distribution was used for adding noise to the orientation, but with a standard deviation of 0.01 radians. In both cases these values
were reached through trial and error.

\subsection{Sense Model}

The next parameter to tune is the sense model. In the sense model, we want to know what the likelihood of a particle being in a certain location is
given the distance measurement from the laser scanner. For this I used a gaussian probability density function with a mean centered around the
laser scanner measurement, and a standard deviation of 1 centimeter. See figure \ref{fig:sense_distribution} for a visualization of what this distribution
would look like if the laser scanner was measuring a distance of 100 centimeters.

\begin{figure}[H]
\includegraphics[width=\linewidth]{imgs/sense_distribution.png}
\caption{Gaussian Distribution for Sense}
\label{fig:sense_distribution}
\end{figure}

From the figure above, we see that a particle reading 100 centimeters would be given a very high likelihood, while a particle with a reading of 96 centimeters would
have almost zero likelihood. These numbers were reached through trial and error. Also one interesting thing to note is that the laser scanner I used in Coppelia acted
as a perfect laser scanner with no noise. Still modeling the sense portion of the algorithm with noise built in is important, because we don't want particles that are
close to the robot to be given zero likelihood even if there is a 100\% chance that the particle guess was wrong.

\subsection{Number of Particles}

The number of particles are a very important part of the performance of the MCL algorithm. I found a higher number of particles would allow the algorithm to converge
on the robot's location faster, and it was also less likely to converge on a bad spot. The downsides with a really high number of particles is the computational
intensity of the algorithm. In the end, through trial and error, I found 300 particles was a good number. It found the robot relatively quickly, and once found did
a good job of maintaining localization. I was also able to run the visualization with 300 particles in real time at approximately 20 frames per second.

\subsection{Percentage of Particles to Regenerate}

As mentioned in the \nameref{sec:resampling} section, we eliminate a certain percentage of particles in each iteration of MCL to avoid particle starvation.
I found the percentage of particles to regenerate was just as important as the number of particles, and more sensitive to tuning. In the end I found that
anywhere between 1-5\% was a good number of particles to regenerate. The upsides to keeping the amount of particles to eliminate so low is that once the robot
is found, the algorithm did a good job of maintaining the localizaton. In the case where I set the percentage high, the algorithm would find the
robot and then a few iterations later would converge on one of the other randomly generated particles before again bouncing back to the robot. The downsides
with keeping the percentage so low is that it takes longer to find the robot because if we initially have a bunch of bad guesses, we will eliminate those guesses
slowly.

\subsection{Sensor Fusion Slop}

One of the parameters not directly related to MCL itself, but rather how we pass data to the MCL algorithm is what I call sensor fusion slop. Since the odometry
measurement and laser range scanner are coming from different ros topics, I had to add a time synchronizer to make sure that the sense measurements were in line
with the movement measurements. The time synchronizer would ensure that the timestamps from the laser range scanner and from the odometry were within a certain
amount of seconds. In the end I found that 0.1 seconds was a good value, and anything much more than that would cause the algorithm to fail. Setting the value to
be lower than 0.1 seconds caused the frequency of incoming measurements to drop and also become inconsistent.

\section{Other Components}

We have talked in great detail about the implementation of the MCL algorithm, but the MCL algorithm is not complete on its own. In the next few subsections I will
briefly discuss the other components required to allow the MCL algorithm to work.

\subsection{Controller}

Without a controller to move the robot and explore the room, the MCL algorithm does not work. The controller I used is extremely simple. I reused the roomba controller
I developed in homework \#2, and just added an additional state called "sweep". The additional state allows the robot to periodically rotate in place approximately
360 degrees. I found this additional state was necessary because I used only a single range sensor. Too many particles would end up with high probability if I
only drove straight, because any particle that was at the same distance from the wall would have a high likelihood. The sweep allowed the robot to sense the various
features of the room, such as the circular obstacles or the corners of the room.

\subsection{Estimation}

The next component needed for MCL to be useful is an estimate of the robot's pose based on the particles generated from MCL. To do this estimation, I used a clustering
algorithm called DBSCAN \cite{DBScan:online}. I performed clustering, chose the cluster with the most number of particles in it, and used the centroid of that cluster
as the location estimate. I did not attempt to estimate the orientation of the robot, but based on the visualization, the particles are almost all oriented the correct
way. It would be interesting to try both taking the average of all the orientations, or just randomly selecting 1 of the particles in the clusters orientation.

\subsection{Visualization}

The next important component is visualization, because one of the most fun parts of MCL is seeing how it works. I first attempted to use matplotlib for visualization,
but I found the performance was not fast enough to generate the plots in real time. After switching to PyQtGraph I no longer had performance issues.

\section{Limitations}

The MCL algorithm has some limitations. First, the MCL algorithm requires a known map. This can be difficult
in real world scenarios. Along similar lines, the algorithm does not adapt when the known map
changes. I tested adding an unknown obstacle to the map, and whenever the robot faces the unknown obstacle
the algorithm falls apart. There are some limitations that are also specific to my implementation. I found even
in the small 5x5 meter room that I tested in, that I needed 100s of particles to obtain a good result. There are
many techniques used to reduce the number of particles needed, but in the end I did not explore them and focused
instead on improving the visualization for the small room.

\section{Conclusions}

The MCL algorithm is very elegant. It is relatively easy to understand, very easy to visualize, and fun to implement.

\subsection{Acknowledgements}
\footnotesize
A thank you to the professor as I relied heavily on the lectures from class on localization, pose representation, dead reckoning
and close-looped control. In addition I also found some interesting techniques for specific parts of the algorithm, such as
low variance sampling from Dr. Stachniss' MCL lecture on youtube \cite{MCLYoutbe:online}. I also used
the MCL wiki \cite{MCLWiki:online} as a reference for the formal definition of the algorithm.


\bibliography{references}

\end{multicols}

\end{document}

